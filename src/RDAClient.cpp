/*
 * RDAClient.cpp
 *
 *  Created on: Feb 5, 2015
 *      Author: mdonze
 */
#include "RDAClient.h"

#include <chrono>
#include <iostream>
#include <string>
#include <thread>

#include <boost/interprocess/ipc/message_queue.hpp>

#include "Constants.h"
#include "RDASource.h"

#include <dic.hxx>

namespace ntof {
namespace proxy {

RDAClient::RDAClient() :
    sourceName("RDA"),
    maxMsg(MAX_MSG),
    maxMsgSize(MAX_MSG_SIZE),
    queueName(MQ_NAME)
{}

RDAClient::~RDAClient()
{
    mq.reset();
}

void RDAClient::postData(pugi::xml_document &doc)
{
    std::ostringstream oss;
    // Uncomment end of line for raw XML output
    doc.save(oss); //,"\t", pugi::format_raw);
    try
    {
        mq->try_send(oss.str().c_str(), oss.str().size(), 0);
    }
    catch (boost::interprocess::interprocess_exception &ex)
    {
        std::cerr << "Failed to send message: " << ex.what() << std::endl;
    }
}

bool RDAClient::loadConfig(const std::string &cfgPath)
{
    pugi::xml_document doc;
    pugi::xml_parse_result res = doc.load_file(cfgPath.c_str());
    if (res)
    {
        pugi::xml_node root = doc.root().child("proxy");
        pugi::xml_node cfg = root.child("configuration");
        if (cfg)
        {
            pugi::xml_node queueCfg = cfg.child("queue");
            if (queueCfg)
            {
                queueName = queueCfg.attribute("name").as_string(MQ_NAME);
                maxMsg = queueCfg.attribute("count").as_int(MAX_MSG);
                maxMsgSize = queueCfg.attribute("size").as_int(MAX_MSG_SIZE);
            }
        }

        mq.reset();
        while (!mq)
        {
            try
            {
                mq.reset(new boost::interprocess::message_queue(
                    boost::interprocess::open_only, queueName.c_str()));
            }
            catch (...)
            {
                std::cerr << "Waiting for message-queue" << std::endl;
                std::this_thread::sleep_for(std::chrono::seconds(3));
            }
        }

        pugi::xml_node sourcesNode = root.child("sources");
        if (sourcesNode)
        {
            for (pugi::xml_node source = sourcesNode.first_child(); source;
                 source = source.next_sibling())
            {
                if (source.attribute("device") && source.attribute("property") &&
                    source.attribute("destination"))
                {
                    sources.push_back(new ntof::proxy::RDASource(
                        source.attribute("device").as_string(""),
                        source.attribute("property").as_string(""),
                        source.attribute("destination").as_string(""),
                        source.attribute("cycle").as_string(""), this));
                }
                else
                {
                    std::cerr << "Bad RDA service description." << std::endl;
                    std::cerr << "Expected device, property and destination "
                                 "attributes"
                              << std::endl;
                }
            }
        }

        // Build the destination sink
        dest.reset(new RDADestination(this));

        pugi::xml_node destinationNodes = root.child("destinations");
        if (destinationNodes)
        {
            for (pugi::xml_node destNode = destinationNodes.first_child();
                 destNode; destNode = destNode.next_sibling())
            {
                if (destNode.attribute("device") &&
                    destNode.attribute("property") &&
                    destNode.attribute("destination"))
                {
                    std::string fromValue = destNode.attribute("device").value();
                    fromValue += '/';
                    fromValue += destNode.attribute("property").value();
                    std::string toValue =
                        destNode.attribute("destination").value();
                    dest->registerNewCommand(fromValue, toValue);
                }
                else
                {
                    std::cerr << "Bad RDA service description." << std::endl;
                    std::cerr << "Expected device, property and destination "
                                 "attributes"
                              << std::endl;
                }
            }
        }
        return true;
    }
    else
    {
        return false;
    }
}

void RDAClient::heartBeat()
{
    while (true)
    {
        std::this_thread::sleep_for(std::chrono::seconds(HEARTBEAT_RATE));
        pugi::xml_document hbDoc;
        pugi::xml_node hbNode = hbDoc.append_child("heartbeat");
        hbNode.append_attribute("source").set_value(sourceName.c_str());
        postData(hbDoc);
    }
}

/**
 * Gets the message queue configuration
 * @param name
 * @param msgMax
 * @param msgMaxSize
 */
void RDAClient::getQueueConfiguration(std::string &name,
                                      std::size_t &msgMax,
                                      std::size_t &msgMaxSize)
{
    name = queueName;
    msgMax = maxMsg;
    msgMaxSize = maxMsgSize;
}

} /* namespace proxy */
} // namespace ntof

/*
 * RDADestination.h
 *
 *  Created on: Feb 9, 2015
 *      Author: mdonze
 */

#ifndef RDADESTINATION_H_
#define RDADESTINATION_H_

#include <string>
#include <thread>

#include <boost/interprocess/ipc/message_queue.hpp>

#include <cmw-rda3/client/service/ClientService.h>

#include "DIMData.h"

namespace ntof {
namespace proxy {
class RDAClient;

class RDADestination
{
public:
    explicit RDADestination(RDAClient *client);
    virtual ~RDADestination();

    RDADestination(const RDADestination &) = delete;
    RDADestination &operator=(const RDADestination &) = delete;

    void consume();
    void registerNewCommand(std::string &from, std::string &to);

private:
    std::string cmdQueueName;
    std::unique_ptr<boost::interprocess::message_queue> mq;
    std::thread consumerThread;
    RDAClient *client_; //!<< Reference to client
    void convertData(ntof::dim::DIMData &dimData,
                     std::unique_ptr<cmw::data::Data> &rdaData);
};

} /* namespace proxy */
} /* namespace ntof */

#endif /* RDADESTINATION_H_ */

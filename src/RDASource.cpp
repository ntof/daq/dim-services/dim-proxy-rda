/*
 * RDASource.cpp
 *
 *  Created on: Jan 15, 2015
 *      Author: mdonze
 */

#include "RDASource.h"

#include <iostream>
#include <vector>

#include <cmw-data/DataExceptions.h>

#include "Constants.h"
#include "DIMData.h"
#include "RDAClient.h"
#include "Utils.h"
#include "pugixml.hpp"

#define RDA_SRC_NAME "RDA"

namespace ntof {
namespace proxy {

std::unique_ptr<cmw::rda3::client::ClientService>
    RDASource::client; //!<< RDA client reference
std::unique_ptr<cmw::RbacLoginService> RDASource::rbacService; //!<< RBAC login
                                                               //!< service
bool RDASource::cmwStarted = false;

RDASource::RDASource(const std::string &deviceName,
                     const std::string &propName,
                     const std::string &dimDest,
                     const std::string &cycle,
                     RDAClient *client) :
    devName(deviceName),
    propertyName(propName),
    destName(dimDest),
    cycleName(cycle),
    client_(client)
{
    pugi::xml_node aqnNode = doc.append_child("acquisition");
    aqnNode.append_attribute("source").set_value(RDA_SRC_NAME);
    fromName = deviceName + "/" + propertyName;
    aqnNode.append_attribute("from").set_value(fromName.c_str());
    aqnNode.append_attribute("to").set_value(destName.c_str());
    client_->postData(doc);
    connect();
}

RDASource::~RDASource()
{
    // stopCMWServices();
    subPtr->unsubscribe();
}

void RDASource::startCMWServices()
{
    if (!cmwStarted)
    {
        if (client.get() == NULL)
        {
            client = cmw::rda3::client::ClientService::create();
        }
        rbacService.reset(new cmw::RbacLoginService(*client));
        rbacService->setLoginPolicy(rbac::LOCATION);
        rbacService->setApplicationName("nTOF info");
        rbacService->setAutoRefresh(true);
        rbacService->start();
        cmwStarted = true;
    }
}

void RDASource::stopCMWServices()
{
    if (cmwStarted)
    {
        rbacService->stop();
        rbacService.reset();
        cmwStarted = false;
    }
}

/**
 * RDA call back
 * @param subscription
 * @param acqData
 * @param updateType
 */
void RDASource::dataReceived(
    const cmw::rda3::client::Subscription & /*subscription*/,
    std::unique_ptr<cmw::rda3::common::AcquiredData> acqData,
    cmw::rda3::common::UpdateType /*updateType*/)
{
    try
    {
        const cmw::data::Data &data = acqData->getData();
        std::vector<std::string> tags = data.getTags();
        doc.reset();
        pugi::xml_node aqnNode = doc.append_child("acquisition");
        aqnNode.append_attribute("source").set_value(RDA_SRC_NAME);
        aqnNode.append_attribute("from").set_value(fromName.c_str());
        aqnNode.append_attribute("to").set_value(destName.c_str());
        pugi::xml_node dataNode = aqnNode.append_child("dataset");
        // Add standard context data
        int i = 0;
        const cmw::rda3::common::AcquiredContext &context = acqData->getContext();
        ntof::dim::DIMData acqStampData(i++, "aqcStamp", "",
                                        context.getAcqStamp());
        acqStampData.insertIntoAqn(dataNode);
        ntof::dim::DIMData cycleStampData(i++, "cycleStamp", "",
                                          context.getCycleStamp());
        cycleStampData.insertIntoAqn(dataNode);
        ntof::dim::DIMData cycleNameData(i++, "cycleName", "",
                                         context.getCycleName());
        cycleNameData.insertIntoAqn(dataNode);

        for (std::vector<std::string>::iterator it = tags.begin();
             it != tags.end(); ++it)
        {
            const cmw::data::Entry *entry = data.getEntry(*it);
            ntof::dim::DIMData *dimData = NULL;
            switch (entry->getType())
            {
            case cmw::data::DT_BYTE:
                dimData = new ntof::dim::DIMData(i, entry->getName(), "",
                                                 entry->getByte());
                break;
            case cmw::data::DT_SHORT:
                dimData = new ntof::dim::DIMData(i, entry->getName(), "",
                                                 entry->getShort());
                break;
            case cmw::data::DT_INT:
                dimData = new ntof::dim::DIMData(i, entry->getName(), "",
                                                 entry->getInt());
                break;
            case cmw::data::DT_LONG:
                dimData = new ntof::dim::DIMData(i, entry->getName(), "",
                                                 entry->getLong());
                break;
            case cmw::data::DT_FLOAT:
                dimData = new ntof::dim::DIMData(i, entry->getName(), "",
                                                 entry->getFloat());
                break;
            case cmw::data::DT_DOUBLE:
                dimData = new ntof::dim::DIMData(i, entry->getName(), "",
                                                 entry->getDouble());
                break;
            case cmw::data::DT_STRING:
                dimData = new ntof::dim::DIMData(
                    i, entry->getName(), "", std::string(entry->getString()));
                break;
            case cmw::data::DT_BOOL:
                dimData = new ntof::dim::DIMData(i, entry->getName(), "",
                                                 entry->getBool());
                break;
            default:
                //                    std::cerr << "RDA source " << fromName <<
                //                    " unsupported datatype for " <<
                //                    entry->getName() << std::endl;
                break;
            }
            ++i;
            if (dimData)
            {
                dimData->insertIntoAqn(dataNode);
                delete dimData;
            }
        }
        client_->postData(doc);
    }
    catch (const cmw::data::DataException &ex)
    {
        doc.reset();
        pugi::xml_node aqnNode = doc.append_child("remove");
        aqnNode.append_attribute("source").set_value(RDA_SRC_NAME);
        aqnNode.append_attribute("from").set_value(fromName.c_str());
        aqnNode.append_attribute("to").set_value(destName.c_str());
        pugi::xml_node errNode = aqnNode.append_child("error");
        errNode.append_child(pugi::node_pcdata).set_value(ex.what());
        std::cerr << "Error: [DataException] " << ex.what() << std::endl;

        client_->postData(doc);
    }
}

/**
 * RDA error call back
 * @param subscription
 * @param exception
 * @param updateType
 */
void RDASource::errorReceived(
    const cmw::rda3::client::Subscription & /*subscription*/,
    std::unique_ptr<cmw::rda3::common::RdaException> exception,
    cmw::rda3::common::UpdateType /*updateType*/)
{
    doc.reset();
    pugi::xml_node aqnNode = doc.append_child("remove");
    aqnNode.append_attribute("source").set_value(RDA_SRC_NAME);
    aqnNode.append_attribute("from").set_value(fromName.c_str());
    aqnNode.append_attribute("to").set_value(destName.c_str());
    pugi::xml_node errNode = aqnNode.append_child("error");
    errNode.append_child(pugi::node_pcdata).set_value(exception->what());
    std::cerr << "Error: [RDAException] " << exception->what() << std::endl;

    client_->postData(doc);
}

void RDASource::connect()
{
    startCMWServices();
    try
    {
        if (cycleName.empty())
        {
            subPtr = client->getAccessPoint(devName, propertyName)
                         .subscribe(NotificationListenerSharedPtr(this));
        }
        else
        {
            subPtr = client->getAccessPoint(devName, propertyName)
                         .subscribe(
                             cmw::rda3::common::RequestContextFactory::create(
                                 cycleName),
                             NotificationListenerSharedPtr(this));
        }
        std::cerr << "Connected on " << devName << std::endl;
    }
    catch (const std::exception &ex)
    {
        std::cerr << "Unable to connect to " << devName << " : " << ex.what()
                  << std::endl;
    }
}

void RDASource::writeProperty(std::string devName,
                              std::string propName,
                              std::unique_ptr<cmw::data::Data> &data)
{
    try
    {
        startCMWServices();
        client->getAccessPoint(devName, propName).set(std::move(data));
    }
    catch (std::exception &ex)
    {
        std::cerr << "Unable to set property... " << ex.what() << std::endl;
    }
}

} /* namespace proxy */
} /* namespace ntof */

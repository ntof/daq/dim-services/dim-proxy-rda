/*
 * RDASource.h
 *
 *  Created on: Jan 15, 2015
 *      Author: mdonze
 */

#ifndef RDASOURCE_H_
#define RDASOURCE_H_

#include <string>

#include <DIMXMLService.h>
#include <cmw-fwk/client/rbac/RbacLoginService.h>
#include <cmw-rda3/client/service/ClientService.h>

using namespace cmw::rda3::client;

namespace ntof {
namespace proxy {
class RDAClient;

class RDASource : public cmw::rda3::client::NotificationListener
{
public:
    RDASource(const std::string &deviceName,
              const std::string &propName,
              const std::string &dimDest,
              const std::string &cycle,
              RDAClient *client);
    virtual ~RDASource();

    /**
     * RDA call back
     * @param subscription
     * @param acqData
     * @param updateType
     */
    virtual void dataReceived(
        const cmw::rda3::client::Subscription &subscription,
        std::unique_ptr<cmw::rda3::common::AcquiredData> acqData,
        cmw::rda3::common::UpdateType updateType) override;

    /**
     * RDA error call back
     * @param subscription
     * @param exception
     * @param updateType
     */
    virtual void errorReceived(
        const cmw::rda3::client::Subscription &subscription,
        std::unique_ptr<cmw::rda3::common::RdaException> exception,
        cmw::rda3::common::UpdateType updateType) override;

    void connect();
    static void writeProperty(std::string devName,
                              std::string propName,
                              std::unique_ptr<cmw::data::Data> &data);

private:
    std::string devName;      //!<< Device name
    std::string propertyName; //!<< Property name
    std::string fromName;     //!<< From data source name
    std::string destName;     //!<< Data destination
    std::string cycleName;    //!<< Cycle name for PPM devices
    RDAClient *client_;       //!<< Client
    pugi::xml_document doc;   //!<< This XML document

    SubscriptionSharedPtr subPtr;

    static std::unique_ptr<cmw::rda3::client::ClientService>
        client; //!<< RDA client reference
    static std::unique_ptr<cmw::RbacLoginService> rbacService; //!<< RBAC login
                                                               //!< service
    static bool cmwStarted;

    static void startCMWServices();
    static void stopCMWServices();
    RDASource(const RDASource &other);            //!<< Don't allow copy
    RDASource &operator=(const RDASource &other); //!<< Don't allow copy
};

} /* namespace proxy */
} /* namespace ntof */

#endif /* RDASOURCE_H_ */

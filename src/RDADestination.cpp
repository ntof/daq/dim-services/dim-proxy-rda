/*
 * RDADestination.cpp
 *
 *  Created on: Feb 9, 2015
 *      Author: mdonze
 */

#include "RDADestination.h"

#include <iostream>

#include "Constants.h"
#include "DIMData.h"
#include "DIMException.h"
#include "RDAClient.h"
#include "RDASource.h"
#include "pugixml.hpp"
namespace ntof {
namespace proxy {

RDADestination::RDADestination(RDAClient *client) : client_(client)
{
    std::string queueName;
    std::size_t maxMsg;
    std::size_t maxMsgSize;
    client_->getQueueConfiguration(queueName, maxMsg, maxMsgSize);
    cmdQueueName = queueName + client_->getSourceName() + "Cmd";
    mq.reset(new boost::interprocess::message_queue(
        boost::interprocess::open_or_create, cmdQueueName.c_str(), maxMsg,
        maxMsgSize));
    consumerThread = std::thread(&RDADestination::consume, this);
}

RDADestination::~RDADestination()
{
    mq.reset();
}

void RDADestination::convertData(ntof::dim::DIMData &dimData,
                                 std::unique_ptr<cmw::data::Data> &rdaData)
{
    switch (dimData.getDataType())
    {
    case ntof::dim::DIMData::TypeBool:
        rdaData->append(dimData.getName(), dimData.getBoolValue());
        break;
    case ntof::dim::DIMData::TypeByte:
        rdaData->append(dimData.getName(), dimData.getByteValue());
        break;
    case ntof::dim::DIMData::TypeDouble:
        rdaData->append(dimData.getName(), dimData.getDoubleValue());
        break;
    case ntof::dim::DIMData::TypeEnum:
        rdaData->append(dimData.getName(), dimData.getEnumValue().getValue());
        break;
    case ntof::dim::DIMData::TypeFloat:
        rdaData->append(dimData.getName(), dimData.getFloatValue());
        break;
    case ntof::dim::DIMData::TypeInt:
        rdaData->append(dimData.getName(), dimData.getIntValue());
        break;
    case ntof::dim::DIMData::TypeLong:
        rdaData->append(dimData.getName(), dimData.getLongValue());
        break;
    case ntof::dim::DIMData::TypeShort:
        rdaData->append(dimData.getName(), dimData.getShortValue());
        break;
    case ntof::dim::DIMData::TypeString:
        rdaData->append(dimData.getName(), dimData.getStringValue().c_str());
        break;
    case ntof::dim::DIMData::TypeUInt:
    case ntof::dim::DIMData::TypeULong:
    case ntof::dim::DIMData::TypeUByte:
    case ntof::dim::DIMData::TypeUShort:
    case ntof::dim::DIMData::TypeInvalid:
    case ntof::dim::DIMData::TypeNested:
        std::cerr << "Unsupported DIM type " << dimData.getName()
                  << " type:" << dimData.getDataType() << std::endl;
        /* unsupported */
        break;
    }
}

void RDADestination::consume()
{
    while (true)
    {
        boost::interprocess::message_queue::size_type recvd_size;
        try
        {
            unsigned int priority;
            std::string rcvString;
            rcvString.resize(mq->get_max_msg_size());
            mq->receive(&rcvString[0], mq->get_max_msg_size(), recvd_size,
                        priority);
            rcvString.resize(recvd_size);
            // Got some data
            pugi::xml_document doc;
            pugi::xml_parse_result parseResult = doc.load_buffer(
                rcvString.c_str(), rcvString.size());
            if (parseResult)
            {
                // XML parsing without errors
                std::string commandName =
                    doc.first_child().attribute("from").value();
                size_t cPos = commandName.find_first_of('/');
                std::string devName = commandName.substr(0, cPos);
                std::string propName = commandName.substr(cPos + 1,
                                                          commandName.size());
                pugi::xml_node params = doc.first_child().first_child();
                std::unique_ptr<cmw::data::Data> data =
                    cmw::data::DataFactory::createData();
                for (pugi::xml_node param = params.first_child(); param;
                     param = param.next_sibling())
                {
                    ntof::dim::DIMData dimData(param);
                    convertData(dimData, data);
                }
                RDASource::writeProperty(devName, propName, data);
            }
            else
            {
                std::cerr << "Error while processing XML command..."
                          << std::endl;
            }
        }
        catch (boost::interprocess::interprocess_exception &ex)
        {
            std::cerr << ex.what() << std::endl;
        }
        catch (ntof::dim::DIMException &ex)
        {
            std::cerr << ex.what() << std::endl;
        }
    }
}

void RDADestination::registerNewCommand(std::string &from, std::string &to)
{
    pugi::xml_document doc;
    pugi::xml_node aqnNode = doc.append_child("command");
    aqnNode.append_attribute("source").set_value(
        client_->getSourceName().c_str());
    aqnNode.append_attribute("from").set_value(from.c_str());
    aqnNode.append_attribute("to").set_value(to.c_str());
    client_->postData(doc);
}

} /* namespace proxy */
} /* namespace ntof */

#
# global makefile
#

CPU ?= L866
CMW_MAKEFILE_VERSION ?= 2.x.x
PARENT_MAKEFILE ?= /acc/local/share/cmw/cmw-makefile/$(CMW_MAKEFILE_VERSION)/Make.cern

PRODUCT=proxy
PROJECT=ntof
BIN_NAME=nTOF_ProxyRDAClient

include $(PARENT_MAKEFILE)






option(TESTS "Compile and run unit tests" OFF)

include(CheckFunctionExists)
check_function_exists(getopt_long HAVE_GETOPT_LONG)

if(TESTS)

    # CppUnit now requires a recent version of C++
    set(CMAKE_CXX_STANDARD 11)

    include(FindPkgConfig)
    pkg_check_modules(CPPUNIT cppunit REQUIRED)

    add_custom_target(test-verbose
        COMMAND ${CMAKE_CTEST_COMMAND} --verbose
        WORKING_DIRECTORY "${CMAKE_BINARY_DIR}")

    enable_testing()

endif()
